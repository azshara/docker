sudo echo 10.12.88.10 hadoop.master >> /etc/hosts
sudo echo 10.12.88.11 hadoop.lucy >> /etc/hosts
sudo echo 10.12.88.12 hadoop.lily >> /etc/hosts

sudo echo 10.11.88.1 zk1 >> /etc/hosts
sudo echo 10.11.88.2 zk2 >> /etc/hosts
sudo echo 10.11.88.3 zk3 >> /etc/hosts

sudo scp /etc/hosts hadoop.lucy:/etc/ 
sudo scp /etc/hosts hadoop.lily:/etc/ 





docker run -d \
 --name hadoop-zk1 \
 -h hadoop.zk1 \
 --net hadoop \
 --ip 10.12.99.1 \
 -e SERVER_ID=1 \
 -e ADDITIONAL_ZOOKEEPER_1=server.1=10.12.99.1:2888:3888 \
 -e ADDITIONAL_ZOOKEEPER_2=server.2=10.12.99.2:2888:3888 \
 -e ADDITIONAL_ZOOKEEPER_3=server.3=10.12.99.3:2888:3888 \
 zookeeper:latest

docker run -d \
 --name hadoop-zk2 \
 -h hadoop.zk2 \
 --net hadoop \
 --ip 10.12.99.2 \
 -e SERVER_ID=2 \
 -e ADDITIONAL_ZOOKEEPER_1=server.1=10.12.99.1:2888:3888 \
 -e ADDITIONAL_ZOOKEEPER_2=server.2=10.12.99.2:2888:3888 \
 -e ADDITIONAL_ZOOKEEPER_3=server.3=10.12.99.3:2888:3888 \
 zookeeper:latest
 
 docker run -d \
 --name hadoop-zk3 \
 -h hadoop.zk3 \
 --net hadoop \
 --ip 10.12.99.3 \
 -e SERVER_ID=3 \
 -e ADDITIONAL_ZOOKEEPER_1=server.1=10.12.99.1:2888:3888 \
 -e ADDITIONAL_ZOOKEEPER_2=server.2=10.12.99.2:2888:3888 \
 -e ADDITIONAL_ZOOKEEPER_3=server.3=10.12.99.3:2888:3888 \
 zookeeper:latest
